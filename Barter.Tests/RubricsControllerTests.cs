﻿using Barter.DAL;
using Barter.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Barter.Controllers;
using System.Web.Mvc;
using System.Collections;

namespace Barter.Tests
{
    [TestClass]
    public class RubricsControllerTests
    {
        [TestMethod]
        public void Get_All_Rubric()
        {
            // Arrange
            
            List<Subrubric> subrubrics = new List<Subrubric>();
            subrubrics.Add(new Subrubric { Id = 1, Name = "Ukranian" });
            subrubrics.Add(new Subrubric { Id = 2, Name = "Hip-hop" });

            var fakerubricks = new List<Rubric>()
            {
              new Rubric { Id = 1, Name = "Sport", Subrubrics = subrubrics },
              new Rubric { Id = 2, Name = "Art", Subrubrics = subrubrics }
            }.AsQueryable();


            Mock<IRepository<Rubric>> mockRepo = new Mock<IRepository<Rubric>>();
            mockRepo.Setup(m => m.GetAll(null)).Returns(fakerubricks);
            
            UnitOfWork uw = new UnitOfWork();
            uw.repositories.Add(typeof(Rubric), mockRepo.Object);

            // Arrange - create the controller
            RubricsController controller = new RubricsController(uw);
            // Act = get the set of categories
            var results = ((IEnumerable<Rubric>)controller.Index().Model).ToArray();

            // Assert
            Assert.AreEqual(results.Length, 2);
            Assert.AreEqual(results[0].Name, "Sport");
            Assert.AreEqual(results[1].Name, "Art");

        }

        [TestMethod]
        public void Can_Add_Rubric()
        {
            // Arrange
            
            List<Subrubric> subrubrics = new List<Subrubric>();
            subrubrics.Add(new Subrubric { Id = 1, Name = "Ukranian" });
            subrubrics.Add(new Subrubric { Id = 2, Name = "Hip-hop" });

            var fakerubricks = new List<Rubric>()
            {
              new Rubric() { Id = 1, Name = "Sport", Subrubrics = subrubrics },
              new Rubric() { Id = 2, Name = "Art", Subrubrics = subrubrics }
            }.AsQueryable();


            Mock<IRepository<Rubric>> mockRepo = new Mock<IRepository<Rubric>>();
            mockRepo.Setup(m => m.GetAll(null)).Returns(fakerubricks);

            UnitOfWork uw = new UnitOfWork();
            uw.repositories.Add(typeof(Rubric), mockRepo.Object);      

            // Arrange - create the controller
            RubricsController controller = new RubricsController(uw);

            Rubric newRubric = new Rubric() { Id = 3, Name = "Education", Subrubrics = subrubrics };
            // Act = get the set of categories
            var result = controller.Create(newRubric);
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult));
            RedirectToRouteResult actionResult = result as RedirectToRouteResult;
            Assert.AreEqual(actionResult.RouteValues["action"], "Index");
        }

        [TestMethod]
        public void Delete_Rubric()
        {
            // Arrange
            
            List<Subrubric> subrubrics = new List<Subrubric>();
            subrubrics.Add(new Subrubric { Id = 1, Name = "Ukranian" });
            subrubrics.Add(new Subrubric { Id = 2, Name = "Hip-hop" });

            var fakerubricks = new List<Rubric>()
            {
              new Rubric() { Id = 1, Name = "Sport", Subrubrics = subrubrics },
              new Rubric() { Id = 2, Name = "Art", Subrubrics = subrubrics }
            };


            Mock<IRepository<Rubric>> mockRepo = new Mock<IRepository<Rubric>>();
            mockRepo.Setup(m => m.GetAll(null)).Returns(fakerubricks.AsQueryable());

            UnitOfWork uw = new UnitOfWork();
            uw.repositories.Add(typeof(Rubric), mockRepo.Object);

            // Arrange - create the controller
            RubricsController controller = new RubricsController(uw);

            // Act = get the set of categories
            var result = controller.Delete(1);

            IDictionary<string, object> viewData =
                   controller.ViewData as IDictionary<string, object>;

            Assert.IsNotNull(viewData);
            Assert.IsTrue(viewData.ContainsKey("rubric"));
            var r = viewData.First(i => i.Key.Contains("rubric"));
            Assert.IsNull(r.Value);
            
        }

        [TestMethod]
        public void DeleteConfirm_Rubric()
        {
            // Arrange
            
            List<Subrubric> subrubrics = new List<Subrubric>();
            subrubrics.Add(new Subrubric { Id = 1, Name = "Ukranian" });
            subrubrics.Add(new Subrubric { Id = 2, Name = "Hip-hop" });

            var fakerubricks = new List<Rubric>()
            {
              new Rubric() { Id = 1, Name = "Sport", Subrubrics = subrubrics },
              new Rubric() { Id = 2, Name = "Art", Subrubrics = subrubrics }
            };


            Mock<IRepository<Rubric>> mockRepo = new Mock<IRepository<Rubric>>();
            mockRepo.Setup(m => m.GetAll(null)).Returns(fakerubricks.AsQueryable());

            UnitOfWork uw = new UnitOfWork();
            uw.repositories.Add(typeof(Rubric), mockRepo.Object);

            // Arrange - create the controller
            RubricsController controller = new RubricsController(uw);

            // Act = get the set of categories
            var result = controller.DeleteConfirmed(1);

            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult));
            RedirectToRouteResult actionResult = result as RedirectToRouteResult;
            Assert.AreEqual(actionResult.RouteValues["action"], "Index");

            IDictionary<string, object> viewData =
                   controller.ViewData as IDictionary<string, object>;

            Assert.IsNotNull(viewData);
            Assert.IsTrue(viewData.ContainsKey("rubrics"));
        }

        [TestMethod]
        public void Details__About_Rubric()
        {
            // Arrange
           
            List<Subrubric> subrubrics = new List<Subrubric>();
            subrubrics.Add(new Subrubric { Id = 1, Name = "Ukranian" });
            subrubrics.Add(new Subrubric { Id = 2, Name = "Hip-hop" });

            var fakerubricks = new List<Rubric>()
            {
              new Rubric() { Id = 1, Name = "Sport", Subrubrics = subrubrics },
              new Rubric() { Id = 2, Name = "Art", Subrubrics = subrubrics }
            };

            Mock<IRepository<Rubric>> mockRepo = new Mock<IRepository<Rubric>>();
            mockRepo.Setup(m => m.GetAll(null)).Returns(fakerubricks.AsQueryable());

            UnitOfWork uw = new UnitOfWork();
            uw.repositories.Add(typeof(Rubric), mockRepo.Object);

            // Arrange - create the controller
            RubricsController controller = new RubricsController(uw);

            // Act = get the set of categories
            //var result = controller.Details(1);

            //IDictionary<string, object> viewData =
            //       controller.ViewData as IDictionary<string, object>;

            //Assert.IsNotNull(viewData);
            //Assert.IsTrue(viewData.ContainsKey("details"));

            var results = (ViewResult)controller.Details(1);

            // Assert
            Assert.IsNotNull(results.ViewData);
            Assert.IsInstanceOfType(results.ViewData.Model, typeof(Rubric));
        }

        [TestMethod]
        public void Edit_Rubric_ByID()
        {
            // Arrange
            
            List<Subrubric> subrubrics = new List<Subrubric>();
            subrubrics.Add(new Subrubric { Id = 1, Name = "Ukranian" });
            subrubrics.Add(new Subrubric { Id = 2, Name = "Hip-hop" });

            var fakerubricks = new List<Rubric>()
            {
              new Rubric() { Id = 1, Name = "Sport", Subrubrics = subrubrics },
              new Rubric() { Id = 2, Name = "Art", Subrubrics = subrubrics }
            };

            Mock<IRepository<Rubric>> mockRepo = new Mock<IRepository<Rubric>>();
            mockRepo.Setup(m => m.GetAll(null)).Returns(fakerubricks.AsQueryable());

            UnitOfWork uw = new UnitOfWork();
            uw.repositories.Add(typeof(Rubric), mockRepo.Object);

            // Arrange - create the controller
            RubricsController controller = new RubricsController(uw);

            // Act = get the set of categories
            var result = controller.Details(1);

            IDictionary<string, object> viewData =
                   controller.ViewData as IDictionary<string, object>;

            Assert.IsNotNull(viewData);
            Assert.IsTrue(viewData.ContainsKey("details"));
        }

        [TestMethod]
        public void Edit_Rubric()
        {
            // Arrange

            List<Subrubric> subrubrics = new List<Subrubric>();
            subrubrics.Add(new Subrubric { Id = 1, Name = "Ukranian" });
            subrubrics.Add(new Subrubric { Id = 2, Name = "Hip-hop" });

            var fakerubricks = new List<Rubric>()
            {
              new Rubric() { Id = 1, Name = "Sport", Subrubrics = subrubrics },
              new Rubric() { Id = 2, Name = "Art", Subrubrics = subrubrics }
            };

            Mock<IRepository<Rubric>> mockRepo = new Mock<IRepository<Rubric>>();
            mockRepo.Setup(m => m.GetAll(null)).Returns(fakerubricks.AsQueryable());

            UnitOfWork uw = new UnitOfWork();
            uw.repositories.Add(typeof(Rubric), mockRepo.Object);

            // Arrange - create the controller
            RubricsController controller = new RubricsController(uw);

            // Act = get the set of categories
            var result = controller.Edit(fakerubricks[0]);
          
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult));
            RedirectToRouteResult actionResult = result as RedirectToRouteResult;
            Assert.AreEqual(actionResult.RouteValues["action"], "Index");
        }
    }
}

