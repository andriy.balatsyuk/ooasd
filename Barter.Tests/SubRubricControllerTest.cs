﻿using Barter.Controllers;
using Barter.DAL;
using Barter.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Barter.Tests
{
    [TestClass]
    public class SubrubricControllerTests
    {
        [TestMethod]
        public void Get_All_Subrubric()
        {
            // Arrange
           
            List<Subrubric> fakeSubrubrics = new List<Subrubric>();
            fakeSubrubrics.Add(new Subrubric { Id = 1, Name = "Ukranian" });
            fakeSubrubrics.Add(new Subrubric { Id = 2, Name = "Hip-hop" });


            Mock<IRepository<Subrubric>> mockRepo = new Mock<IRepository<Subrubric>>();
            mockRepo.Setup(m => m.GetAll(null)).Returns(fakeSubrubrics.AsEnumerable());

            UnitOfWork uw = new UnitOfWork();
            uw.repositories.Add(typeof(Subrubric), mockRepo.Object);

            // Arrange - create the controller
            SubrubricsController controller = new SubrubricsController(uw);
            // Act = get the set of categories
            var results = ((IEnumerable<Subrubric>)controller.Index().Model).ToArray();

            // Assert
            Assert.AreEqual(results.Length, 2);
            Assert.AreEqual(results[0].Name, "Ukranian");
            Assert.AreEqual(results[1].Name, "Hip-hop");
        }

        [TestMethod]
        public void Add_SubRubric()
        {
            // Arrange

            List<Subrubric> fakeSubrubrics = new List<Subrubric>();
            fakeSubrubrics.Add(new Subrubric { Id = 1, Name = "Ukranian" });
            fakeSubrubrics.Add(new Subrubric { Id = 2, Name = "Hip-hop" });

            Mock<IRepository<Subrubric>> mockRepo = new Mock<IRepository<Subrubric>>();
            mockRepo.Setup(m => m.GetAll(null)).Returns(fakeSubrubrics.AsEnumerable());

            UnitOfWork uw = new UnitOfWork();
            uw.repositories.Add(typeof(Subrubric), mockRepo.Object);

            // Arrange - create the controller
            SubrubricsController controller = new SubrubricsController(uw);

            Subrubric newRubric = new Subrubric() { Id = 3, Name = "Math"};
            // Act = get the set of categories
            var result = controller.Create(newRubric);
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult));
            RedirectToRouteResult actionResult = result as RedirectToRouteResult;
            Assert.AreEqual(actionResult.RouteValues["action"], "Index");
        }

        [TestMethod]
        public void Delete_Subrubric()
        {
            // Arrange

            List<Subrubric> fakeSubrubrics = new List<Subrubric>();
            fakeSubrubrics.Add(new Subrubric { Id = 1, Name = "Ukranian" });
            fakeSubrubrics.Add(new Subrubric { Id = 2, Name = "Hip-hop" });

            Mock<IRepository<Subrubric>> mockRepo = new Mock<IRepository<Subrubric>>();
            mockRepo.Setup(m => m.GetAll(null)).Returns(fakeSubrubrics.AsQueryable());

            UnitOfWork uw = new UnitOfWork();
            uw.repositories.Add(typeof(Subrubric), mockRepo.Object);

            // Arrange - create the controller
            SubrubricsController controller = new SubrubricsController(uw);

            // Act = get the set of categories
            var result = controller.Delete(1);

            IDictionary<string, object> viewData =
                   controller.ViewData as IDictionary<string, object>;

            Assert.IsNotNull(viewData);
            Assert.IsTrue(viewData.ContainsKey("subrubric"));
            var r = viewData.First(i => i.Key.Contains("subrubric"));
            Assert.IsNull(r.Value);

        }

        [TestMethod]
        public void DeleteConfirm_Subrubric()
        {
            // Arrange

            List<Subrubric> fakeSubrubrics = new List<Subrubric>();
            fakeSubrubrics.Add(new Subrubric { Id = 1, Name = "Ukranian" });
            fakeSubrubrics.Add(new Subrubric { Id = 2, Name = "Hip-hop" });

            Mock<IRepository<Subrubric>> mockRepo = new Mock<IRepository<Subrubric>>();
            mockRepo.Setup(m => m.GetAll(null)).Returns(fakeSubrubrics.AsQueryable());

            UnitOfWork uw = new UnitOfWork();
            uw.repositories.Add(typeof(Subrubric), mockRepo.Object);

            // Arrange - create the controller
            SubrubricsController controller = new SubrubricsController(uw);

            // Act = get the set of categories
            var result = controller.DeleteConfirmed(1);

            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult));
            RedirectToRouteResult actionResult = result as RedirectToRouteResult;
            Assert.AreEqual(actionResult.RouteValues["action"], "Index");

            IDictionary<string, object> viewData =
                   controller.ViewData as IDictionary<string, object>;

            Assert.IsNotNull(viewData);
            Assert.IsTrue(viewData.ContainsKey("subrubrics"));
        }

        [TestMethod]
        public void Details__About_Subrubric()
        {
            // Arrange
            
            List<Rubric> fakerubrics = new List<Rubric>();
            fakerubrics.Add(new Rubric { Id = 1, Name = "Education" });
            fakerubrics.Add(new Rubric { Id = 2, Name = "Sport" });

            List<Subrubric> fakeSubrubrics = new List<Subrubric>();
            fakeSubrubrics.Add(new Subrubric { Id = 1, Name = "Ukranian", RubricId = 1, Rubric = fakerubrics[0] });
            fakeSubrubrics.Add(new Subrubric { Id = 2, Name = "Hip-hop", RubricId = 2, Rubric = fakerubrics[1] });

            Mock<IRepository<Rubric>> mockRepoRub = new Mock<IRepository<Rubric>>();
            mockRepoRub.Setup(m => m.GetAll(null)).Returns(fakerubrics.AsQueryable());

            Mock<IRepository<Subrubric>> mockRepo = new Mock<IRepository<Subrubric>>();
            mockRepo.Setup(m => m.GetAll(null)).Returns(fakeSubrubrics.AsQueryable());

            UnitOfWork uw = new UnitOfWork();         
            uw.repositories.Add(typeof(Rubric), mockRepoRub.Object);
            uw.repositories.Add(typeof(Subrubric), mockRepo.Object);

            // Arrange - create the controller
            SubrubricsController controller = new SubrubricsController(uw);

            // Act = get the set of categories
            var results = (ViewResult)controller.Details(1); 

            // Assert
            Assert.IsNotNull(results.ViewData);
            Assert.IsInstanceOfType(results.ViewData.Model, typeof(Subrubric));
            //Assert.AreEqual(results.Name, "Ukranian");
        }

        [TestMethod]
        public void Edit_Rubric_ByID()
        {
            // Arrange

            List<Subrubric> fakeSubrubrics = new List<Subrubric>();
            fakeSubrubrics.Add(new Subrubric { Id = 1, Name = "Ukranian"});
            fakeSubrubrics.Add(new Subrubric { Id = 2, Name = "Hip-hop" });

            Mock<IRepository<Subrubric>> mockRepo = new Mock<IRepository<Subrubric>>();
            mockRepo.Setup(m => m.GetAll(null)).Returns(fakeSubrubrics.AsQueryable());

            UnitOfWork uw = new UnitOfWork();
            uw.repositories.Add(typeof(Subrubric), mockRepo.Object);

            // Arrange - create the controller
            SubrubricsController controller = new SubrubricsController(uw);

            // Act = get the set of categories
            
            var subrubric = (Subrubric)controller.Edit(1).Model;

            // Assert
            Assert.AreEqual(subrubric.Id, 1);
            Assert.AreEqual(subrubric.Name, "Ukranian");
           
        }

        [TestMethod]
        public void Edit_Rubric()
        {
            // Arrange

            List<Subrubric> fakeSubrubrics = new List<Subrubric>();
            fakeSubrubrics.Add(new Subrubric { Id = 1, Name = "Ukranian" });
            fakeSubrubrics.Add(new Subrubric { Id = 2, Name = "Hip-hop" });

            Mock<IRepository<Subrubric>> mockRepo = new Mock<IRepository<Subrubric>>();
            mockRepo.Setup(m => m.GetAll(null)).Returns(fakeSubrubrics.AsQueryable());

            UnitOfWork uw = new UnitOfWork();
            uw.repositories.Add(typeof(Subrubric), mockRepo.Object);

            // Arrange - create the controller
            SubrubricsController controller = new SubrubricsController(uw);

            // Act = get the set of categories
            var result = controller.Edit(fakeSubrubrics[0]);
          
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult));
            RedirectToRouteResult actionResult = result as RedirectToRouteResult;
            Assert.AreEqual(actionResult.RouteValues["action"], "Index");
        }
    }
}