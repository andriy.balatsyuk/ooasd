﻿using Barter.DAL;
using Barter.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Barter.Startup))]
namespace Barter
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            createRolesandUsers();
        }

        private void createRolesandUsers()
        {
            BarterContext context = new BarterContext();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));


            // In Startup iam creating first Admin Role and creating a default Admin User    
            if (!roleManager.RoleExists("Admin"))
            {

                // first we create Admin rool   
                var role = new IdentityRole("Admin");
                roleManager.Create(role);

                //creating a ApplicationUser
                var user = new ApplicationUser();
                user.UserName = "Admin@gmail.com";
                user.FName = "Admin";
                user.LName = "Admin";
                user.Age = 30;
                user.Hometown = "Lviv";
                user.Email = "Admin@gmail.com";


                string userPWD = "be_carefuL1";

                var chkUser = UserManager.Create(user, userPWD);
                if (chkUser.Succeeded)
                {
                    var result1 = UserManager.AddToRole(user.Id, "Admin");
                }

            }

            // creating Moderator role    
            if (!roleManager.RoleExists("Moderator"))
            {
                var role = new IdentityRole("Moderator");
                roleManager.Create(role);
            }

            // creating User role    
            if (!roleManager.RoleExists("User"))
            {
                var role = new IdentityRole("User");
                roleManager.Create(role);

            }
        }

        
    }
}
