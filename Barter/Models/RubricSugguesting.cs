﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Barter.Models
{
    public class RubricSugguesting
    {
        public int RubricSugguestingId { get; set; }

        [Required]
        public string Name { get; set; }

        public string UserSugguestingId { get; set; }

        [ForeignKey("UserSugguestingId")]
        public ApplicationUser UserSugguesting { get; set; }

    }
}