﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Barter.Models
{
    public class RequestViewModel
    {
        // skill you are sugguesting
        [Display(Name = "Your skill")]
        public int SkillIdFrom { get; set; }

        public string Message { get; set; }

    }
}