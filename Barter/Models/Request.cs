﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Barter.Models
{
    // в БД дописати тригер-перевірку, що відбуватиметься перед Insert.
    //  Тригер перевірятиме чи SkillTo.User.UserSkillWanted.Contains( SkillFrom.SubrubricId )
    // Тобто щоб наперед перевірити чи зацікавлений юзер, якому надсилають запит, у даному бартері.
    public class Request
    {
        [Key]
        public int Id { get; set; }

        public int SkillIdFrom { get; set; }

        [ForeignKey("SkillIdFrom")]
        public virtual UserSkill SkillFrom { get; set; }

        public int SkillIdTo { get; set; }

        [ForeignKey("SkillIdTo")]
        public virtual UserSkill SkillTo { get; set; }

        public string Message { get; set; }

        /// <summary>
        /// -1 means request was turned down
        /// 0 means request has not been considered yet
        /// 1 means request was submited
        /// </summary>
        [Range(-1, 1)]
        public int Status { get; set; }

        public DateTime DateOfSending { get; set; }

    }
}