﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Barter.Models
{
    public class UserSkillViewModel
    {
        [Required]
        public int SubrubricId { get; set; }
       
        [Required]
        [Display(Name = "Key word")]
        public string Name { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }
    }
}