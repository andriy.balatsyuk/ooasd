﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Barter.Models
{
    // в бізнес-лог. дописати перевірку "чи запит від цього юзера був підтверджений, (якщо не був, або запитати взагалі не було, то даний юзер не може залишати свій фідбек під даним скілом)"
    public class Feedback
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(240, ErrorMessage = "Максимальна довжина коментаря {1}", MinimumLength = 1)]
        public string Comment { get; set; }

        [Required]
        public bool Like { get; set; }

        [Required]
        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }

        public int UserSkillId { get; set; }

        [ForeignKey("UserSkillId")]
        public virtual UserSkill UserSkill { get; set; }

        public DateTime DateOfFeedback { get; set; }

    }
}