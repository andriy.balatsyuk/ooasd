﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Barter.Models
{
    public class SubrubricSuguesting
    {
        public int SubrubricSugguestingId { get; set; }

        [Required]
        public string Name { get; set; }

        public string UserSugguestingId { get; set; }

        [ForeignKey("UserSugguestingId")]
        public ApplicationUser UserSugguesting { get; set; }

        public int RubricId { get; set; }

        [ForeignKey("RubricId")]
        public Rubric Rubric { get; set; }
    }
}