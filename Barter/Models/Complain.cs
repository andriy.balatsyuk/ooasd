﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Barter.Models
{
    public class Complain
    {
        public int ComplainId { get; set; }

        // відгук, на який жаліються
        public int FeedbackId { get; set; }

        [ForeignKey("FeedbackId")]
        public Feedback Feedback { get; set; }

        // пояснююче повідомлення
        [Required]
        public string Message { get; set; }

        // юзер, що жаліється
        public string UserComplainingId { get; set; }

        [ForeignKey("UserComplainingId")]
        public ApplicationUser UserComplaining { get; set; }


    }
}