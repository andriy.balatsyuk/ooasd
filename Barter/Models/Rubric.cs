﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Barter.Models
{
    public class Rubric
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "Максимальна довжина {2}", MinimumLength = 2)]
        [Display(Name = "Rubric")]
        public string Name { get; set; }


        // navigation property
        public virtual ICollection<Subrubric> Subrubrics { get; set; }
    }
}