﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Barter.Models
{
    public class UserSkillWanted
    {
        [Key]
        public int Id { get; set; }

        public int SubrubricId { get; set; }

        [ForeignKey("SubrubricId")]
        public virtual Subrubric Subrubric { get; set; }

        [Required]
        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser ApplicationUser { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }
    }
}