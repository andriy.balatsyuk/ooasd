﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Barter.DAL;
using Barter.Models;
using Microsoft.AspNet.Identity;

namespace Barter.Controllers
{
    public class RequestsController : Controller
    {
        private UnitOfWork unitOfWork = null;

        public RequestsController(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public RequestsController()
        {
            unitOfWork = new UnitOfWork();
        }

        // GET: Requests
        public ActionResult Index()
        {
            return View(unitOfWork.Repository<Request>().GetAll() as IEnumerable<Request>);
        }

        // GET: Requests/Details/5
        public ActionResult Details(int id = 0)
        {
            Request request = unitOfWork.Repository<Request>().Get(c => c.Id == id);
            if (request == null)
            {
                return HttpNotFound();
            }
            return View(request);
        }

        // GET: Requests/Create
        [HttpGet]
        public ActionResult Create(int UserSkillToId)
        {
           // ViewBag.SkillToName = skill.Name;
            ViewBag.UserId = Guid.Parse(User.Identity.GetUserId());

            ViewBag.SkillToName = unitOfWork.Repository<UserSkill>().Get(s => s.Id == UserSkillToId).Name;

            ViewBag.SkillIdFrom = new SelectList(unitOfWork.Repository<UserSkill>().GetAll(sk => sk.UserId == User.Identity.GetUserId()) as IEnumerable<UserSkill>, "Id", "Name");
            return View();
        }

        // POST: Requests/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RequestViewModel requestView, int UserSkillToId = 0)
        {
            if (ModelState.IsValid)
            {
                string userId = User.Identity.GetUserId();

                var request = new Request()
                {
                    SkillIdFrom = requestView.SkillIdFrom,
                    SkillIdTo = UserSkillToId,
                    Message = requestView.Message,
                    DateOfSending = DateTime.Now,
                    Status = 0
                };
                unitOfWork.Repository<Request>().Add(request);
                unitOfWork.SaveChanges();
                return RedirectToAction("Index");
            }
            return View();
        }

        [HttpGet]
        public ActionResult Accept(int requestId)
        {
            Request request = unitOfWork.Repository<Request>().Get(c => c.Id == requestId);
            if (request == null)
            {
                return HttpNotFound();
            }

            request.Status = 1;
            unitOfWork.Repository<Request>().Attach(request);
            unitOfWork.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Deny(int requestId)
        {
            Request request = unitOfWork.Repository<Request>().Get(c => c.Id == requestId);
            if (request == null)
            {
                return HttpNotFound();
            }

            request.Status = -1;
            unitOfWork.Repository<Request>().Attach(request);
            unitOfWork.SaveChanges();
            return RedirectToAction("Index");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
