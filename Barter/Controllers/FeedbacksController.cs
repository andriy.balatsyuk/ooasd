﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Barter.DAL;
using Barter.Models;
using Microsoft.AspNet.Identity;

namespace Barter.Controllers
{
    public class FeedbacksController : Controller
    {
        private UnitOfWork unitOfWork = null;

        public FeedbacksController()
        {
            unitOfWork = new UnitOfWork();
        }

        public FeedbacksController(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        // GET: Feedbacks   --- передати айдішку скіла
        public ActionResult Index(int userSkillId = 0)
        {
            return View(unitOfWork.Repository<Feedback>().GetAll() as IEnumerable<Feedback>);
        }

        // GET: Feedbacks/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Feedbacks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        // -- перевірити чи може коментити
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( Feedback feedback, int userSkillId)
        {
            if (feedback.Comment != String.Empty)
            {
                feedback.DateOfFeedback = DateTime.Now;
                feedback.UserSkillId = userSkillId;
                feedback.UserId = User.Identity.GetUserId();

                unitOfWork.Repository<Feedback>().Add(feedback);
                unitOfWork.SaveChanges();
                return RedirectToAction("Index");
            }
            
            return View(feedback);
        }

        //// GET: Feedbacks/Edit/5
        public ActionResult Edit(int id = 0)
        {
            Feedback feedback = unitOfWork.Repository<Feedback>().Get(c => c.Id == id);
            // якщо юзер написав даний коментар, то може його редагувати
            if(User.Identity.GetUserId() == feedback.UserId)
            {
                return View(feedback);
            }
            // дивиться далі коментарі
            return RedirectToAction("Index");
            
        }

        // POST: Feedbacks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( Feedback feedback)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.Repository<Feedback>().Attach(feedback);
                unitOfWork.SaveChanges();
                return RedirectToAction("Index");
            }
   
            return View(feedback);
        }

        //// GET: Feedbacks/Delete/5
        public ActionResult Delete(int id = 0)
        {
            Feedback feedback = unitOfWork.Repository<Feedback>().Get(c => c.Id == id);
            if (feedback.UserId == User.Identity.GetUserId())
            {
                unitOfWork.Repository<Feedback>().Delete(feedback);
                unitOfWork.SaveChanges();
            }
            return RedirectToAction("Index");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
