﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Barter.DAL;
using Barter.Models;
using Microsoft.AspNet.Identity;

namespace Barter.Controllers
{
    public class UserSkillWantedsController : Controller
    {
        private UnitOfWork unitOfWork = null;

        public UserSkillWantedsController()
        {
            unitOfWork = new UnitOfWork();
        }

        public UserSkillWantedsController(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        // GET: UserSkillWanted
        public ActionResult Index()
        {
            return View(unitOfWork.Repository<UserSkillWanted>().GetAll() as IEnumerable<UserSkillWanted>);
        }

        // GET: UserSkillWanted/Details/5
        public ActionResult Details(int id = 0)
        {
            UserSkillWanted skill = unitOfWork.Repository<UserSkillWanted>().Get(c => c.Id == id);
            if (skill == null)
            {
                return HttpNotFound();
            }
            return View(skill);
        }

        // GET: UserSkillWanted/Create
        public ActionResult Create()
        {
            ViewBag.UserId = Guid.Parse(User.Identity.GetUserId());
            ViewBag.SubrubricId = new SelectList(unitOfWork.Repository<Subrubric>().GetAll() as IEnumerable<Subrubric>, "Id", "Name");
            return View();
        }

        // POST: UserSkillWanteds/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UserSkillViewModel userSkill)
        {
            if (ModelState.IsValid)
            {
                string userId = User.Identity.GetUserId();

                var skill = new UserSkillWanted()
                {
                    Name = userSkill.Name,
                    Description = userSkill.Description,
                    UserId = userId,
                    SubrubricId = userSkill.SubrubricId
                };
                unitOfWork.Repository<UserSkillWanted>().Add(skill);
                unitOfWork.SaveChanges();
                return RedirectToAction("Index");
            }
            return View();
        }

        // GET: UserSkills/Edit/5
        public ActionResult Edit(int id = 0)
        {
            UserSkillWanted skill = unitOfWork.Repository<UserSkillWanted>().Get(c => c.Id == id);
            if (skill == null)
            {
                return HttpNotFound();
            }
            ViewBag.SubrubricId = new SelectList(unitOfWork.Repository<Subrubric>().GetAll() as IEnumerable<Subrubric>, "Id", "Name", skill.SubrubricId);
            return View(skill);

        }

        // POST: UserSkills/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserSkillWanted userSkill)
        {
            if (userSkill.Name != string.Empty)
            {
                unitOfWork.Repository<UserSkillWanted>().Attach(userSkill);
                unitOfWork.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(userSkill);

        }

        // GET: UserSkills/Delete/5
        public ActionResult Delete(int id = 0)
        {
            UserSkillWanted skill = unitOfWork.Repository<UserSkillWanted>().Get(c => c.Id == id);
            if (skill == null)
            {
                return HttpNotFound();
            }
            return View(skill);
        }

        // POST: UserSkills/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UserSkillWanted skill = unitOfWork.Repository<UserSkillWanted>().Get(c => c.Id == id);
            unitOfWork.Repository<UserSkillWanted>().Delete(skill);
            unitOfWork.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
