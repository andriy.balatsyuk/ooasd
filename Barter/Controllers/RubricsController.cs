﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Barter.DAL;
using Barter.Models;

namespace Barter.Controllers
{
    public class RubricsController : Controller
    {
        private UnitOfWork unitOfWork = null;

        public RubricsController()
        {
            unitOfWork = new UnitOfWork();
        }

        public RubricsController(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        // GET: Rubrics
        public ViewResult Index()
        {
            return View(unitOfWork.Repository<Rubric>().GetAll() as IEnumerable<Rubric>);
        }

        // GET: Rubrics/Details/5
        public ActionResult Details(int id = 0)
        {

            Rubric rubric = unitOfWork.Repository<Rubric>().Get(c => c.Id == id);
            var rub = unitOfWork.Repository<Rubric>().GetAll().First(c => c.Id == id);
            ViewData["details"] = rubric;

            //if (rubric == null)
            //{
            //    return HttpNotFound();
            //}

            return View(rubric);
        }

        // GET: Rubrics/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Rubrics/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name")] Rubric rubric)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.Repository<Rubric>().Add(rubric);
                unitOfWork.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(rubric);
        }

        // GET: Rubrics/Edit/5
        public ViewResult Edit(int id = 0)
        {
            Rubric rubric = unitOfWork.Repository<Rubric>().Get(c => c.Id == id);
            ViewData["edit"] = rubric;

            //if (rubric == null)
            //{
            //    return HttpNotFound();
            //}

            return View(rubric);
        }

        // POST: Rubrics/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name")] Rubric rubric)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.Repository<Rubric>().Attach(rubric);
                unitOfWork.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(rubric);
        }

        // GET: Rubrics/Delete/5
        public ActionResult Delete(int id = 0)
        {
            Rubric rubric = unitOfWork.Repository<Rubric>().Get(c => c.Id == id);
            ViewData["rubric"] = rubric;

            if (rubric == null)
            {
                return HttpNotFound();
            }

            return View(rubric);
        }

        // POST: Rubrics/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Rubric rubric = unitOfWork.Repository<Rubric>().Get(c => c.Id == id);
            unitOfWork.Repository<Rubric>().Delete(rubric);
            unitOfWork.SaveChanges();
            ViewData["rubrics"] = rubric;
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
