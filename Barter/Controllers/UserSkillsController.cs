﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using Microsoft.AspNet.Identity;
using System.Web.Mvc;
using Barter.DAL;
using Barter.Models;

namespace Barter.Controllers
{
    public class UserSkillsController : Controller
    {
        private UnitOfWork unitOfWork = null;

        public UserSkillsController()
        {
            unitOfWork = new UnitOfWork();
        }

        public UserSkillsController(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        // GET: UserSkills
        public ActionResult Index()
        {
            return View(unitOfWork.Repository<UserSkill>().GetAll() as IEnumerable<UserSkill>);
        }

        // GET: UserSkills/Details/5
        public ActionResult Details(int id = 0)
        {
            UserSkill skill = unitOfWork.Repository<UserSkill>().Get(c => c.Id == id);
            if (skill == null)
            {
                return HttpNotFound();
            }
            return View(skill);
        }

        // GET: UserSkills/Create
        public ActionResult Create()
        {
            ViewBag.UserId = Guid.Parse(User.Identity.GetUserId());
            ViewBag.SubrubricId = new SelectList(unitOfWork.Repository<Subrubric>().GetAll() as IEnumerable<Subrubric>, "Id", "Name");
            return View();
        }

        // POST: UserSkills/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( UserSkillViewModel userSkill)
        {
            if (ModelState.IsValid)
            {
                string userId = User.Identity.GetUserId();

                var skill = new UserSkill() { Name = userSkill.Name,
                                              Description = userSkill.Description,
                                              UserId = userId,
                                              SubrubricId = userSkill.SubrubricId
                                             };
                unitOfWork.Repository<UserSkill>().Add(skill);
                unitOfWork.SaveChanges();
                return RedirectToAction("Index");
            }
            return View();
        }

        // GET: UserSkills/Edit/5
        public ActionResult Edit(int id = 0)
        {
            UserSkill skill = unitOfWork.Repository<UserSkill>().Get(c => c.Id == id);
            if (skill == null)
            {
                return HttpNotFound();
            }
            ViewBag.SubrubricId = new SelectList(unitOfWork.Repository<Subrubric>().GetAll() as IEnumerable<Subrubric>, "Id", "Name", skill.SubrubricId);
            return View(skill);
            
        }

        // POST: UserSkills/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserSkill userSkill)
        {
            if (userSkill.Name != string.Empty)
            {
                unitOfWork.Repository<UserSkill>().Attach(userSkill);
                unitOfWork.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(userSkill);
          
        }

        [HttpGet]
        [ValidateAntiForgeryToken]
        public ActionResult MakeRequest(int id =0)
        {
            UserSkill skill = unitOfWork.Repository<UserSkill>().Get(c => c.Id == id);
            if (skill == null)
            {
                return HttpNotFound();
            }
            ViewBag.SkillToName = skill.Name;
            return RedirectToAction("Create", "RequestController", new { UserSkillToId = id });
        }

        // GET: UserSkills/Delete/5
        public ActionResult Delete(int id = 0)
        {
            UserSkill skill = unitOfWork.Repository<UserSkill>().Get(c => c.Id == id);
            if (skill == null)
            {
                return HttpNotFound();
            }
            return View(skill);
        }

        // POST: UserSkills/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UserSkill skill = unitOfWork.Repository<UserSkill>().Get(c => c.Id == id);
            unitOfWork.Repository<UserSkill>().Delete(skill);
            unitOfWork.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Feedback(int userSkillid = 0)
        {
            return RedirectToAction("Create", "Feedbacks", new { userSkillId = userSkillid });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
