﻿using Barter.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace Barter.DAL
{
    public class BarterContext : IdentityDbContext<ApplicationUser>
    {
        public BarterContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            
            //base.OnModelCreating(modelBuilder);

            //modelBuilder.Entity<Request>()
            //            .HasRequired(t => t.SkillFrom)
            //            .WithMany(t => t.OutputRequests)
            //            .WillCascadeOnDelete(true);

            //modelBuilder.Entity<Request>()
            //           .HasRequired(t => t.SkillTo)
            //           .WithMany(t => t.Requests)
            //           .WillCascadeOnDelete(true);


            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

           // modelBuilder.Entity<Subrubric>().HasRequired(e => e.Rubric).WithMany(e => e.Subrubrics);
        }

        public static BarterContext Create()
        {
            return new BarterContext();
        }

        

    }
}